#!/bin/sh

if [ $# -ne 1 ] || [ "$1" == "main" ] || [ "$1" == "testnet" ] || [ "$1" == "regtest" ] ; then
   echo 'Usage: sh quick-install-waykicoind.sh $NET_TYPE'
   echo ' '
   echo ' NET_TYPE: main | testnet | regtest'
   echo ' '
fi

NET_TYPE=$1
WORK_DIR=~/wicc-docker-$NET_TYPE

mkdir -p $WORK_DIR/bin $WORK_DIR/conf $WORK_DIR/data
docker pull wicc/waykicoind

cat > /tmp/WaykiChain.conf <<EOF
rpcuser=wiccadmin
rpcpassword=wiccpass
blockminsize=1000
zapwallettxes=0
debug=INFO
debug=ERROR
logprinttoconsole=0
logtimestamps=1
logprinttofile=1
logprintfileline=1
server=1
listen=1
uiport=4555
rpcport=6968
rpcallowip=*.*.*.*
isdbtraversal=1
disablesafemode=1
gen=0
genproclimit=1000000
rpcthreads=8
EOF

cp /tmp/WaykiChain.conf $WORK_DIR/conf/
[ "$NET_TYPE" != "main" ] && echo "${NET_TYPE}=1" >> $WORK_DIR/conf/WaykiChain.conf

cat > $WORK_DIR/bin/run-waykicoind.sh <<EOF
cd $WORK_DIR && \
docker run --privileged=true --name waykicoind-${NET_TYPE} -p 8920:8920 -p 6968:6968 \
  -v `pwd`/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
  -v `pwd`/data:/root/.WaykiChain/${NET_TYPE} \
  -v `pwd`/bin:/opt/wicc/bin \
  -d wicc/waykicoind
EOF

echo "Clean up waykicoind-${NET_TYPE} container if existing..."
docker stop waykicoind-${NET_TYPE} && docker rm waykicoind-${NET_TYPE}

echo "Launch docker waykicoind-${NET_TYPE} now..."
sh $WORK_DIR/bin/run-waykicoind.sh

echo "Installed with success. You can start interacting with the node now. "
echo " "
echo "E.g. $ docker exec -it waykicoind-${NET_TYPE} coind getinfo "
echo " "
